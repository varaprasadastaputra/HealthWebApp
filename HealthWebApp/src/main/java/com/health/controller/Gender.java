package com.health.controller;

import java.math.BigDecimal;

public enum Gender {

    /*
    * Since its been told that for mail 2 percent should be add
    * */
    MALE(new BigDecimal(2)),


    /*
    * There is no extra percentage  for FEMALE
    * */
    FEMALE(BigDecimal.ZERO),


    /*
    * There is no extra percentage  for OTHERS
    * */
    OTHER(BigDecimal.ZERO);

    private BigDecimal bigDecimal;

    Gender(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;

    }

    public BigDecimal getBigDecimal() {
        return bigDecimal;
    }

    public void setBigDecimal(BigDecimal bigDecimal) {
        this.bigDecimal = bigDecimal;
    }
}
