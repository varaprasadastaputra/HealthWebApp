package com.health.controller;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestPremiumCalc{

    @Test()
    public void testPremiumCalc() {
        List<InputPojo> inputPojoList = new ArrayList<>();
        Calculator calculator = new Calculator();
        BigDecimal premium = BigDecimal.ZERO;

        inputPojoList.add(new InputPojo("Norman Gomes",new BigDecimal(34), Gender.MALE,false,false,false,true,false,true,true,false,new BigDecimal(6856)));
        inputPojoList.add(new InputPojo("Sample1",new BigDecimal(25), Gender.MALE,false,true,false,true,false,true,false,true,new BigDecimal(6067)));
        inputPojoList.add(new InputPojo("Sample3",new BigDecimal(40), Gender.OTHER,true,true,true,true,false,false,false,false,new BigDecimal(7618)));
        inputPojoList.add(new InputPojo("Sample4",new BigDecimal(50), Gender.MALE,false,false,false,false,true,true,false,true,new BigDecimal(116739)));

        for(InputPojo inputPojo : inputPojoList){
            premium = calculator.computePremium(inputPojo);
            Assert.assertEquals(premium.intValue(),inputPojo.getPremiumn().intValue());
        }

    }
}
