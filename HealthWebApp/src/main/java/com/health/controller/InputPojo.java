package com.health.controller;

import java.io.Serializable;
import java.math.BigDecimal;

public class InputPojo implements Serializable {

    private String name;
    private BigDecimal age;
    private BigDecimal premiumn;
    private Gender gender;

    private Boolean hypertension;
    private Boolean bloodPressure;
    private Boolean bloodsugar;
    private Boolean overweight;
    private Boolean smoking;
    private Boolean alcohol;
    private Boolean dailyExercise;
    private Boolean drugs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getHypertension() {
        return hypertension;
    }

    public void setHypertension(Boolean hypertension) {
        this.hypertension = hypertension;
    }

    public Boolean getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(Boolean bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public Boolean getBloodsugar() {
        return bloodsugar;
    }

    public void setBloodsugar(Boolean bloodsugar) {
        this.bloodsugar = bloodsugar;
    }

    public Boolean getOverweight() {
        return overweight;
    }

    public void setOverweight(Boolean overweight) {
        this.overweight = overweight;
    }

    public Boolean getSmoking() {
        return smoking;
    }

    public void setSmoking(Boolean smoking) {
        this.smoking = smoking;
    }

    public Boolean getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Boolean alcohol) {
        this.alcohol = alcohol;
    }

    public Boolean getDailyExercise() {
        return dailyExercise;
    }

    public void setDailyExercise(Boolean dailyExercise) {
        this.dailyExercise = dailyExercise;
    }

    public Boolean getDrugs() {
        return drugs;
    }

    public void setDrugs(Boolean drugs) {
        this.drugs = drugs;
    }

    public InputPojo() {}

    public InputPojo(String name, BigDecimal age, Gender gender, Boolean hypertension, Boolean bloodPressure, Boolean bloodsugar, Boolean overweight, Boolean smoking, Boolean alcohol, Boolean dailyExercise, Boolean drugs) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.hypertension = hypertension;
        this.bloodPressure = bloodPressure;
        this.bloodsugar = bloodsugar;
        this.overweight = overweight;
        this.smoking = smoking;
        this.alcohol = alcohol;
        this.dailyExercise = dailyExercise;
        this.drugs = drugs;
    }
    public InputPojo(String name, BigDecimal age, Gender gender, Boolean hypertension, Boolean bloodPressure, Boolean bloodsugar, Boolean overweight, Boolean smoking, Boolean alcohol, Boolean dailyExercise, Boolean drugs,BigDecimal premiumn) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.hypertension = hypertension;
        this.bloodPressure = bloodPressure;
        this.bloodsugar = bloodsugar;
        this.overweight = overweight;
        this.smoking = smoking;
        this.alcohol = alcohol;
        this.dailyExercise = dailyExercise;
        this.drugs = drugs;
        this.premiumn = premiumn;
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public BigDecimal getPremiumn() {
        return premiumn;
    }

    public void setPremiumn(BigDecimal premiumn) {
        this.premiumn = premiumn;
    }

    public BigDecimal getAge() {
        return age;
    }
}
