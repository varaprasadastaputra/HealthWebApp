package com.health.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Controller
public class BaseController {

	private static int counter = 0;
	private static final String VIEW_INDEX = "index";
	private final static Logger logger = LoggerFactory.getLogger(BaseController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {
		return VIEW_INDEX;
	}

	@RequestMapping(value = "/value", method = RequestMethod.GET)
	public String welcomeName(@RequestParam(value = "name") String name,
							  @RequestParam(value = "age") BigDecimal age,
							  @RequestParam(value = "gender") Gender gender,
							  @RequestParam(value = "hypertension") Boolean hypertension,
							  @RequestParam(value = "bloodPressure") Boolean bloodPressure,
							  @RequestParam(value = "bloodsugar") Boolean bloodsugar,
							  @RequestParam(value = "overweight") Boolean overweight,
							  @RequestParam(value = "smoking") Boolean smoking,
							  @RequestParam(value = "alcohol") Boolean alcohol,
							  @RequestParam(value = "dailyExercise") Boolean dailyExercise,
							  @RequestParam(value = "drugs") Boolean drugs,ModelMap model) {


		InputPojo inputPojo = new InputPojo(name, age, gender, hypertension, bloodPressure, bloodsugar,
				overweight, smoking, alcohol, dailyExercise, drugs);
		Calculator calculator = new Calculator();
		BigDecimal premium = calculator.computePremium(inputPojo);
		model.addAttribute("premium", premium.intValue());

		return VIEW_INDEX;

	}

/*tried making a post call but couldn't do it coz of JSON converter problem*/
	/*@RequestMapping(value = "/valuepost", method = RequestMethod.POST)
	public String welcomeName(@RequestBody InputPojo inputPojo) {


//		model.addAttribute("name", "name" + name);
//		model.addAttribute("age", "age" + age);
//		model.addAttribute("gender", "gender" + gender);
//		model.addAttribute("counter", ++counter);
		logger.debug("[welcomeName] {} , {} ,{} ", inputPojo);
		return "bINGOOO";

	}*/

}