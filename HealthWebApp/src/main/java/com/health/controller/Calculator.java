package com.health.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Calculator {
    private final static Logger logger = LoggerFactory.getLogger(Calculator.class);
    final BigDecimal ONE = new BigDecimal(1);
    final BigDecimal THREE = new BigDecimal(3);
    final BigDecimal TEN = new BigDecimal(10);
    final BigDecimal HUNDERED = new BigDecimal(100);
    final BigDecimal GREATER_THAN_40 = new BigDecimal(20);
    final BigDecimal BASE_VALUE = new BigDecimal(5000);

    protected BigDecimal computePremium(InputPojo inputPojo) {

        BigDecimal premium = calcPremiumBasedOnAge(inputPojo.getAge());
        BigDecimal totPercentage = BigDecimal.ZERO;

        premium = premium.add(premium.multiply(inputPojo.getGender().getBigDecimal().divide(new BigDecimal(100))));

        if(inputPojo.getHypertension())     premium = calcPremium(premium,ONE);
        if(inputPojo.getBloodPressure())    premium = calcPremium(premium,ONE);
        if(inputPojo.getBloodsugar())       premium = calcPremium(premium,ONE);
        if(inputPojo.getOverweight())       premium = calcPremium(premium,ONE);

        if(inputPojo.getSmoking())          totPercentage = totPercentage.add(THREE);
        if(inputPojo.getAlcohol())          totPercentage = totPercentage.add(THREE);
        if(inputPojo.getDrugs())            totPercentage = totPercentage.add(THREE);


        if(inputPojo.getDailyExercise())totPercentage = totPercentage.subtract(THREE);

        if(totPercentage.compareTo(BigDecimal.ZERO) != 0) premium = calcPremium(premium,totPercentage);

        premium = premium.setScale(0, BigDecimal.ROUND_UP);

        logger.debug("Name : {} Age : {} Gender : {} Hypertension : {} BloodPressure : {} BloodSugar : {} Overweight : {}" +
                "Smoking : {} alcohol : {} DailyExercise : {} Drugs : {} Premium : {}",inputPojo.getName(),inputPojo.getAge(),inputPojo.getGender(),inputPojo.getHypertension(),inputPojo.getBloodPressure(),
                inputPojo.getBloodsugar(),inputPojo.getOverweight(),inputPojo.getSmoking(),inputPojo.getAlcohol(),inputPojo.getDailyExercise(),inputPojo.getDrugs(),premium);
        return premium;
    }

    private BigDecimal calcPremium(BigDecimal premium,BigDecimal percentage) {
        return premium.add(premium.multiply(percentage.divide(HUNDERED)));
    }

    private BigDecimal calcPremiumBasedOnAge(BigDecimal age) {
        BigDecimal premium = new BigDecimal(5000);
        int i=0;

        if(age.intValue() >= 18 && age.intValue() <= 25)  i=1;

        if(age.intValue() > 25 && age.intValue() <= 30)  i=2;

        if(age.intValue() > 30 && age.intValue() <= 35)  i=3;

        if(age.intValue() > 35 && age.intValue() <= 40)  i=4;

        for(int j =1; j<= i ; j++){
            premium = premium.add(premium.multiply(TEN.divide(HUNDERED)));
        }

        if(age.intValue() > 40){
            premium = premium.add(premium.multiply(GREATER_THAN_40));
        }

        return premium;
    }
}
